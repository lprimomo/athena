# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# This module is used to set up the environment for SFGen
# 
#

# Set the environment variable(s):
find_package( SFGen )

if( SFGEN_FOUND )
  set( SFGENENVIRONMENT_ENVIRONMENT 
        FORCESET SFGENVER ${SFGEN_LCGVERSION}
        FORCESET SFGENPATH ${SFGEN_LCGROOT} 
        APPEND LHAPATH
              /cvmfs/atlas.cern.ch/repo/sw/Generators/lhapdfsets/current
        APPEND LHAPDF_DATA_PATH
              /cvmfs/atlas.cern.ch/repo/sw/Generators/lhapdfsets/current )
endif()


# Silently declare the module found:
set( SFGENENVIRONMENT_FOUND TRUE )


